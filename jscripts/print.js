function print_ms()
{
	var OLECMDID_PRINT = 6;
	var OLECMDEXECOPT_DONTPROMPTUSER = 2;
	var OLECMDEXECOPT_PROMPTUSER = 1;
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';

	function deal()
	{
		WebBrowser1.outerHTML = "";
		window.onerror = oldHandler;
		return true;
	}

	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);

	oldHandler = window.onerror;
	window.onerror = deal;

	WebBrowser1.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER);
	WebBrowser1.outerHTML = "";
	window.onerror = oldHandler;
}
